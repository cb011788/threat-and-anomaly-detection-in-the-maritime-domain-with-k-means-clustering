
import os
import conda

conda_file_dir = conda.__file__
conda_dir = conda_file_dir.split('lib')[0]
proj_lib = os.path.join(os.path.join(conda_dir, 'share'), 'proj')
os.environ["PROJ_LIB"] = proj_lib
from mpl_toolkits.basemap import Basemap
import matplotlib
import matplotlib.pyplot as plt

# This is for the backend of matplot lib
matplotlib.use("TkAgg")


import sqlite3 as lite
import pandas as pd
import numpy as np

import ais_package

''' Testing
from_date = '10/10/2015'
to_date = '17/10/2015'
output_path = '.'
choosen_columns= ais_package.choosen_columns
static_file_path = 'nari_static.csv'
dynamic_file_path = 'nari_dynamic.csv'
'''

''' Handling the on click event to run the program'''
def clicked():
    from_date = '10/10/2015'
    to_date = '17/10/2015'
    output_path = 'Outputs'
    column_choice = ais_package.column_choice
    static_file_path = 'nari_static.csv'
    dynamic_file_path = 'nari_dynamic.csv'

    ais_package.detect_abnormals(static_file_path,
                                 dynamic_file_path,
                                 from_date,
                                 to_date,
                                 output_path,
                                 column_choice,
                                 )
    print ('DB')

databasepath = ('geotest2.db')
conn = lite.connect(databasepath)
c = conn.cursor()

#c.execute('''CREATE TABLE results4 (position int,speedoverground float,courseoverground float,trueheading int,lon float,lat float,sourcemmsi int,clusters int,abnormal int,abnormal_confidence int)''')
conn.commit()

df = pd.read_csv("result.csv")
df.to_sql('results4', conn, if_exists='append', index=False)

normalPoints = "SELECT lat, lon, abnormal FROM results4 WHERE sourcemmsi = 228041600;"
#abnormalPoints = "SELECT lat, lon FROM results4 WHERE sourcemmsi = 228041600 AND abnormal = 1;"

with conn:
    dbquery = pd.read_sql_query(normalPoints, conn)
conn.close()

# Working basemap
def Graph():
    m = Basemap(projection='mill',
                llcrnrlat= 45,
                llcrnrlon= -10,
                urcrnrlat = 51,
                urcrnrlon= 0,
                resolution= 'f')

    m.drawcoastlines()
    m.bluemarble()

    x, y = m(dbquery['lon'], dbquery['lat'])

    colors = np.where(dbquery["abnormal"] == 1, 'r', 'g')

    m.scatter(x, y, 0.1, marker='o', c=colors)

    plt.show()



# Working multiple classes
from tkinter import *
from tkinter.ttk import *
import tkinter as tk
from tkinter import ttk

LARGEFONT = ("Verdana", 35)


class tkinterApp(tk.Tk):

    # __init__ function for class tkinterApp
    def __init__(self, *args, **kwargs):
        # __init__ function for class Tk
        tk.Tk.__init__(self, *args, **kwargs)

        # creating a container
        container = tk.Frame(self)
        container.pack(side="top", fill="both", expand=True)

        container.grid_rowconfigure(0, weight=1)
        container.grid_columnconfigure(0, weight=1)

        # initializing frames to an empty array
        self.frames = {}

        # iterating through a tuple consisting
        # of the different page layouts
        for F in (StartPage, Page1, Page2):
            frame = F(container, self)

            # initializing frame of that object from
            # startpage, page1, page2 respectively with
            # for loop
            self.frames[F] = frame

            frame.grid(row=0, column=0, sticky="nsew")

        self.show_frame(StartPage)

    # to display the current frame passed as
    # parameter
    def show_frame(self, cont):
        frame = self.frames[cont]
        frame.tkraise()


# first window frame startpage
class StartPage(tk.Frame):
    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)

        # label of frame Layout 2
        label = ttk.Label(self, text="Threat and Anomaly Detection", font=LARGEFONT)

        # putting the grid in its place by using
        # grid
        label.grid(row=0, column=4, padx=10, pady=10)

        button1 = ttk.Button(self, text="Algorithm Tuning",
                             command=lambda: controller.show_frame(Page1))


        # putting the button in its place by
        # using grid
        button1.grid(row=1, column=1, padx=10, pady=10)

        ## button to show frame 2 with text layout2
        button2 = ttk.Button(self, text="Visualisation",
                             command=lambda: controller.show_frame(Page2))

        # putting the button in its place by
        # using grid
        button2.grid(row=2, column=1, padx=10, pady=10)


''' Configuration Page'''
class Page1(tk.Frame):

    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)

        ''' Navigate to the start page '''
        # Layout2
        button1 = ttk.Button(self, text="Home",
                             command=lambda: controller.show_frame(StartPage))

        # Putting the button in its place
        # By using grid
        button1.grid(row=0, column=1)

        l = Label(self, text="Select a reference case for K-Means clustering ")
        l.grid(row = 1, column = 1)

        rad1 = Radiobutton(self, text='Ship Type', value=1)

        rad2 = Radiobutton(self, text='Navigational Status', value=2)

        rad1.grid(column=0, row=5,padx=10, pady=10)

        rad2.grid(column=2, row=5,padx=10, pady=10)

        comb = tk.Label(self,
                        text="Select Ship Type: ")
        comb.grid(column=0, row=7, padx=10, pady=10)

        combo = ttk.Combobox(self)

        combo['values'] = ("Fishing", "Underwater Operations", "Diving Operations", "Military Operations", "Law Enforcement", "Search and Rescue", "Cargo", "Passenger", "Tanker", "Medical Transport", "Pleasure Craft", "Towing")

        combo.current(3)  # set the selected item

        combo.grid(row=7, column=2, padx=10, pady=10)

        comb2 = tk.Label(self,
                        text="Select Navigational Status: ")
        comb2.grid(column=0, row=9, padx=10, pady=10)

        combo2 = ttk.Combobox(self)

        combo2['values'] = (
        "Under way using its engine", "Anchored", "Not under command", "Has restricted maneuverability", "Ship draught is limiting its movement", "Moored (tied to another object to limit free movement)", "Engaged in fishing", "Under way sailing", "hips carrying dangerous goods/harmful substances/marine pollutants", "Power-driven vessel towing astern", " Power-driven vessel pushing ahead/towing alongside", "Any of the following are active: AIS-SART (Search and Rescue Transmitter), AIS-MOB (Man Overboard), AIS-EPIRB (Emergency Position Indicating Radio Beacon)", "Undefined")

        combo2.current(6)  # set the selected item

        combo2.grid(row=9, column=2, padx=10, pady=10)

        datelabel = Label(self, text="Enter time period in the format XX/YY/YYYY (Leave text boxes blank to run algorithm on the full dataset [6 months])")

        lbl = Label(self, text="Date from: ")
        entry1 = tk.Entry(self)
        lbl2 = Label(self, text="Date to: ")
        entry2 = tk.Entry(self)

        datelabel.grid(row = 10,column=1, padx=10, pady=10)
        lbl.grid(row=11, column=0, padx=10, pady=10)
        entry1.grid(row=11, column=1, padx=10, pady=10)
        lbl2.grid(row=12, column=0, padx=10, pady=10)
        entry2.grid(row=12, column=1, padx=10, pady=10)

        lb3 = Label(self, text="Train model with fake data?")

        rad3 = Radiobutton(self, text='No', value=1)

        rad4 = Radiobutton(self, text='Yes', value=2)

        lb3.grid(row=13, column=0, padx=10, pady=10)
        rad3.grid(row=13, column=1, padx=10, pady=10)
        rad4.grid(row=13, column=2, padx=10, pady=10)

        runTheModel = tk.Button(self, text="Run the model", command=lambda: clicked())
        runTheModel.grid(row=15, column=1, padx=10, pady=10)

        button3 = ttk.Button(self, text="Show Visualisation Page",
                             command=lambda: controller.show_frame(Page2))

        button3.grid(row=16, column=1, padx=10, pady=10)

        ''' Events handling:'''




# third window frame page2
class Page2(tk.Frame):
    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        label = ttk.Label(self, text="Anomaly Visualisation", font=LARGEFONT)
        label.grid(row=0, column=4, padx=10, pady=10)

        # button to show frame 2 with text
        # layout2
        button1 = ttk.Button(self, text="Algorithm Tuning",
                             command=lambda: controller.show_frame(Page1))

        # putting the button in its place by
        # using grid
        button1.grid(row=1, column=1, padx=10, pady=10)

        # button to show frame 3 with text
        # layout3
        button2 = ttk.Button(self, text="Startpage",
                             command=lambda: controller.show_frame(StartPage))

        # putting the button in its place by
        # using grid
        button2.grid(row=2, column=1, padx=10, pady=10)


        button3 = ttk.Button(self, text="Visualisation",
                             command=lambda: Graph())

        button3.grid(row=5, column=1, padx=10, pady=10)

# Driver Code
app = tkinterApp()
app.mainloop()


