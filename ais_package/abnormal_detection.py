import pandas as pd
import random
import datetime
from sklearn.cluster import KMeans
from scipy.spatial.distance import cdist
import numpy as np

def read_data(static_file_path, dynamic_file_path):
    '''
    read data files and convert to pandas dataframe
    '''
    static_df = pd.read_csv(static_file_path, header=0, engine='python')
    dynamic_df = pd.read_csv(dynamic_file_path, header=0, engine='python')

    ## Drop the empty rows (Pre-processing)
    dynamic_df = dynamic_df.fillna(0)
    dynamic_df.isnull().any()
    dynamic_df = dynamic_df.astype(float)
    return static_df, dynamic_df


def filter_ship_by_date(dynamic_df, static_df, from_date, to_date):
    '''
    Passed date format : "20/01/2020" "d/m/y"
    Required conversion : Unix timestamp
    '''
    if from_date == '' or to_date == '':
        return dynamic_df

    element = datetime.datetime.strptime(from_date, "%d/%m/%Y")
    timestamp_from = datetime.datetime.timestamp(element)

    element = datetime.datetime.strptime(to_date, "%d/%m/%Y")
    timestamp_to = datetime.datetime.timestamp(element)

    filtered_ship = dynamic_df[dynamic_df['t'] >= timestamp_from]
    filtered_ship = filtered_ship[filtered_ship['t'] <= timestamp_to]

    return filtered_ship


def apply_kmeans(feature_matrix, num_clusters):
    # Apply K-Means
    km = KMeans(n_clusters=num_clusters)
    km.fit(feature_matrix)
    clusters = km.labels_.tolist()
    centers = km.cluster_centers_

    ## Calculat the distance of each point to each cluster
    dist_matrix = cdist(feature_matrix, centers)

    ## Get the distance of each point to its cluster
    centroid_dist = []
    for i in range(len(clusters)):
        centroid_dist.append(dist_matrix[i][clusters[i]])

    ## Calculate the mean of distance and standard deviation of point distances for each cluster
    clusters_dist_mean = []
    clusters_dist_std = []
    for k in range(len(set(clusters))):
        temp_dist = []
        for i in range(len(dist_matrix)):
            if clusters[i] == k:
                temp_dist.append(dist_matrix[i][k])
        mean_dist = 0
        std_dist = 0
        if len(temp_dist) > 0:
            mean_dist = np.mean(temp_dist)
            std_dist = np.std(temp_dist)
        clusters_dist_std.append(std_dist)
        clusters_dist_mean.append(mean_dist)

    '''
    Returned values:
    Clusters: Clusters is a list of all the identified clusters.
    Centroid_dist: A 2D array of the distances of each data point to its relative cluster 
    Clusters_dist_mean: A 1D array populated with the mean point distances for each cluster. 
    Clusters_dist_std: A 1D array populated with the standard deviation of point distances for each cluster.

    '''
    return clusters, centroid_dist, clusters_dist_mean, clusters_dist_std


def random_floats(low, high, size):
    '''
    Generate an n = size randome number from a specific range within the lower and upper bounds of the column values [low,high]
    '''

    return [random.uniform(low, high) for _ in range(size)]


def processing_data(column_choice, dynamic_df, grouped_filtered_data, grouped_column, generate_fake_data=False):
    # Comment this out to process data without fake injected values, as the default passed is False, but is over written by the subsequent line
    generate_fake_data = True

    total_ship_data = []
    print(f"processing total of {len(grouped_filtered_data)} batch")
    # For each ship_mmsi (Primary Key)

    for ship_idx, ship_filtered in enumerate(grouped_filtered_data):
        print(f"processing data {ship_idx + 1} {ship_filtered} >>")

        # Get all the data related to specific ship_mmsi
        ship_df = dynamic_df[dynamic_df[grouped_column] == ship_filtered]
        ship_df = ship_df.reset_index()

        # Get the time values
        timestamp_column = ship_df['t'].values.tolist()

        # Delete all the columns in the dataframe that are not included in the column_choice (Features)
        for column in ship_df.columns:
            if column not in column_choice:
                del ship_df[column]

        # Drop duplicate values
        temp_df = ship_df.drop_duplicates(keep=False, inplace=False)

        if len(ship_df) > 50:
            # Fake data injection
            if generate_fake_data:
                # Randomly 10% fake data from total of ship data, this decimal can be changed for tuning purposes
                fake_data_len = int(0.1 * len(ship_df))
                fake_data = {}
                for column in ship_df.columns:
                    ## Randomly inject fake data from inside min-max range of column data
                    fake_data[column] = random_floats(ship_df[column].min(), ship_df[column].max(), fake_data_len)

                fake_data = pd.DataFrame(fake_data)
                ship_df = pd.concat([ship_df, fake_data])

            ## Set the cluster number (K)
            cluster_num = int(len(temp_df) / 4)

            ## Apply kmeans on the ship data frame
            clusters, centroid_dist, clusters_dist_mean, clusters_dist_std = apply_kmeans(ship_df.values, num_clusters=cluster_num)

            ship_df['grouped_column'] = ship_filtered
            ship_df['clusters'] = clusters
            ship_df['abnormal'] = 0
            ship_df['abnormal_confidence'] = 0

            ## Insert a new column called 'fake-data' into the data frame to mark the injected fake data points as 1 and the actual points as 0
            if generate_fake_data:
                is_fake_data = [0] * len(ship_df)
                is_fake_data[-fake_data_len:] = [1] * fake_data_len
                ship_df['fake-data'] = is_fake_data
                timestamp_fake = [0] * fake_data_len
                timestamp_column.extend(timestamp_fake)

            ## Abnormal clusters
            # Detect the clusters (with the points in them) that have less than 3 points as Abnormal
            cluters_counts = ship_df['clusters'].value_counts()
            abnormal_clusters = [cluster for cluster, volume in cluters_counts.items()
                                 if volume < 3]
            abnormal_idx = ship_df[ship_df['clusters'].isin(abnormal_clusters)].index
            ship_df.loc[abnormal_idx, 'abnormal'] = 1
            ship_df.loc[abnormal_idx, 'abnormal_confidence'] = 1.0

            ## Abnormal points
            ## For data points that are more than 3 times the standard deviation away from the mean of the point distances for the cluster, then points are likely to be outliers (3-Sigma Implementation)
            abnormal_points = ship_df['abnormal'].values.tolist()
            abnormal_points_confidences = ship_df['abnormal_confidence'].values.tolist()
            for i, dist in enumerate(centroid_dist):
                clusters_dist_cut_off = clusters_dist_std[clusters[i]] * 3
                clusters_dist_lower = clusters_dist_mean[clusters[i]] - clusters_dist_cut_off
                clusters_dist_upper = clusters_dist_mean[clusters[i]] + clusters_dist_cut_off
                if centroid_dist[i] < clusters_dist_lower:
                    abnormal_points[i] = 1
                    abnormal_points_confidences[i] = (clusters_dist_lower - centroid_dist[i]) / (
                                clusters_dist_lower + centroid_dist[i])
                elif centroid_dist[i] > clusters_dist_upper:
                    abnormal_points[i] = 1
                    abnormal_points_confidences[i] = (centroid_dist[i] - clusters_dist_upper) / (
                                centroid_dist[i] + clusters_dist_upper)

                if abnormal_points_confidences[i] > 1:
                    abnormal_points_confidences[i] = 1

            ship_df['t'] = timestamp_column
            ship_df['abnormal'] = abnormal_points
            ship_df['abnormal_confidence'] = abnormal_points_confidences

            total_ship_data.append(ship_df)

    total_ship_df = pd.concat(total_ship_data)
    return total_ship_df

# 5 Dimensions - Can be changed to smaller dimensions for faster processing, for example, just lon and lat
column_choice = ['lon', 'lat', 'trueheading', 'courseoverground', 'speedoverground']

def groupby_data(ships_df, grouped_column):
    grouped_data = list(set(ships_df[grouped_column].values.tolist()))
    return grouped_data

def detect_abnormals(static_file_path, dynamic_file_path,
                     from_date='', to_date='',
                     output_path='.',
                     chosen_columns=column_choice,
                     ):
    static_df, dynamic_df = read_data(static_file_path, dynamic_file_path)
    filtered_ships = filter_ship_by_date(dynamic_df, static_df, from_date, to_date)
    grouped_column = 'sourcemmsi'  #This can be changed to navigational status, this would be very computationally intensive for the full data set
    group_data = groupby_data(filtered_ships, grouped_column)
    processed_ship_df = processing_data(chosen_columns, dynamic_df, group_data, grouped_column=grouped_column)
    processed_ship_df.to_csv(f'{output_path}/result.csv')

    return processed_ship_df

detect_abnormals(static_file_path = 'Cargo.csv',
                            dynamic_file_path = 'nari_dynamic.csv',
                            from_date = '20/10/2015',
                            to_date = '21/10/2015',
                            output_path = '',
                            chosen_columns = ['lon','lat','trueheading','courseoverground','speedoverground']
                            )



