import ais_package
import argparse

if __name__ == "__main__":

    # Initialize parser
    parser = argparse.ArgumentParser()
    
    parser.add_argument("-s", "--Static_file", help = "enter static file path")
    parser.add_argument("-d", "--Dynamic_file", help = "enter dynamic file path")
    parser.add_argument("-f", "--From_date", help = "enter from date")
    parser.add_argument("-t", "--To_date", help = "enter to date")
    parser.add_argument("-c", "--Columns", help = "enter the chosen columns seperated by a comma")
    parser.add_argument("-o", "--Output", help = "enter output path, or leave blank to output to current directory")

    # Read arguments from command line
    args = parser.parse_args()

    if not args.Static_file:
        print("please insert Static_file parameter")
        exit()
    if not args.Dynamic_file:
        print("please insert Dynamic_file parameter")
        exit()
    
    static_file_path = args.Static_file
    dynamic_file_path = args.Dynamic_file
    from_date = '10/10/2015' if not args.From_date else args.From_date
    to_date = '17/10/2015' if not args.To_date else args.To_date
    output_path = '.' if not args.Output else args.Output
    column_choice= ais_package.choosen_columns if not args.Columns else (str(args.Columns)).split(',')

    for column in column_choice:
        if column not in ais_package.choosen_columns:
            print(f"ERROR this column does not exist '{column}' ")
            exit()
    
    ais_package.detect_abnormals(static_file_path = static_file_path,
                                 dynamic_file_path = dynamic_file_path,
                                 from_date = from_date,
                                 to_date = to_date,
                                 output_path = output_path,
                                 chosen_columns = column_choice,
                                 )
